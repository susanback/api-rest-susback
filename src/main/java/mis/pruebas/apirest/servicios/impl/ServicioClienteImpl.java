package mis.pruebas.apirest.servicios.impl;

import ch.qos.logback.core.net.SyslogOutputStream;
import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCliente;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.UnaryOperator;


@Service
public class ServicioClienteImpl implements ServicioCliente {
    public final Map<String,Cliente> clientes = new ConcurrentHashMap<String,Cliente>();

    @Override
    public List<Cliente> obtenerClientes() {
        return List.copyOf(this.clientes.values());
    }

    @Override
    public void insertarClienteNuevo(Cliente cliente) {
        this.clientes.put(cliente.documento, cliente);
    }

    @Override
    public Cliente obtenerCliente(String documento) {
        return this.clientes.get(documento);
    }

    @Override
    public void guardarCliente(Cliente cliente) {
        this.clientes.replace(cliente.documento, cliente);
    }

    @Override
    public void emparcharCliente(Cliente parche) {
        final Cliente existente = this.clientes.get(parche.documento);

        if(parche.edad != existente.edad)
            existente.edad = parche.edad;

        if(parche.nombre != null)
            existente.nombre = parche.nombre;

        if(parche.correo != null)
            existente.correo = parche.correo;

        if(parche.direccion != null)
            existente.direccion = parche.direccion;

        if(parche.telefono != null)
            existente.telefono = parche.telefono;

        if(parche.fechaNacimiento != null)
            existente.fechaNacimiento = parche.fechaNacimiento;

        this.clientes.replace(existente.documento, existente);
    }

    @Override
    public void borrarCliente(String documento) {
        if (!this.clientes.containsKey(documento))
            throw new RuntimeException("No existe el cliente: " + documento);
        this.clientes.remove(documento);
    }

    @Override
    public void agregarCuentaCliente(String documento, Cuenta cuenta) {
        final Cliente cliente =this.obtenerCliente(documento);
        cliente.cuentas.add(cuenta);
    }

    @Override
    public List<Cuenta> obtenerCuentasCliente(String documento) {
        final Cliente cliente = this.obtenerCliente(documento);
        return cliente.cuentas;
    }

    @Override
    public void reemplazarCuentaCliente(String documento, Cuenta cuenta) {

        final Cliente cliente =this.obtenerCliente(documento);

        cliente.cuentas.forEach((a)->{
            System.out.println(a.numero);
            if (a.numero.equals(cuenta.numero) )
                cliente.cuentas.set(cliente.cuentas.indexOf(a), cuenta);
        });

    }

    @Override
    public void emparcharCuentaCliente(String documento, Cuenta parche) {
        final Cliente cliente =this.obtenerCliente(documento);

        cliente.cuentas.forEach((existente)->{
            if (existente.numero.equals(parche.numero) )

            if(parche.numero != existente.numero)
                existente.numero = parche.numero;

            if(parche.moneda != null)
                existente.moneda = parche.moneda;

            if(parche.estado != null)
                existente.estado = parche.estado;

            if (parche.saldo != null)
                existente.saldo = parche.saldo;

            if (parche.oficina != null)
                existente.oficina = parche.oficina;

            if (parche.tipo != null)
                existente.tipo = parche.tipo;

            //cliente.cuentas.set(cliente.cuentas.indexOf(existente), parche);

        });


    }

    @Override
    public void borrarCuentasCliente(String documento, String cuenta) {
        final Cliente cliente = this.obtenerCliente(documento);

        cliente.cuentas.forEach((del) -> {
            if (del.numero.equals(cuenta))
                cliente.cuentas.remove(del);

        });
    }

}

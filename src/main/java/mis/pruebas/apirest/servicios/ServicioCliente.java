package mis.pruebas.apirest.servicios;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;

import java.util.List;

public interface ServicioCliente {

    // CRUD

    public List<Cliente> obtenerClientes();

    // CREATE
    public void insertarClienteNuevo(Cliente cliente);

    // READ
    public Cliente obtenerCliente(String documento);

    // UPDATE (solamente modificar, no crear).
    public void guardarCliente(Cliente cliente);
    public void emparcharCliente(Cliente parche);

    // DELETE
    public void borrarCliente(String documento);


    // METODOS DE CLIENTEST EN RELACION A CUENTAS
    public void agregarCuentaCliente(String documento, Cuenta cuenta);
    //CONSULTA
    public List<Cuenta> obtenerCuentasCliente(String documento);

    // UPDATE
    public void reemplazarCuentaCliente(String documento, Cuenta cuenta);
    public void emparcharCuentaCliente(String documento, Cuenta parche);

    // DELETE
    public void borrarCuentasCliente(String documento, String cuenta);

}

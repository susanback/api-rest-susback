package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(Rutas.CLIENTES + "/{documento}/cuentas")
public class ControladorCuentaCliente {

    @Autowired
    ServicioCliente servicioCliente;

    // POST http://localhost:9000/api/v1/clientes/12345678/cuentas + DATOS
    @PostMapping
    public ResponseEntity agregarCuentaCliente(@PathVariable String documento,
                                               @RequestBody Cuenta cuenta) {
        try {
            this.servicioCliente.agregarCuentaCliente(documento, cuenta);
        } catch(Exception x) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    // GET http://localhost:9000/api/v1/clientes/12345678/cuentas
    @GetMapping
    public ResponseEntity<List<Cuenta>> obtenerCuentasCliente(@PathVariable String documento) {

        try{
            final List<Cuenta> cuentasCliente = this.servicioCliente.obtenerCuentasCliente(documento);
            return ResponseEntity.ok(cuentasCliente);
        } catch (Exception x) {
            return ResponseEntity.notFound().build();
        }

    }

    // POST http://localhost:9000/api/v1/clientes/12345678/cuentas + DATOS
    @PutMapping
    public ResponseEntity reemplazarCuentaCliente(@PathVariable String documento,
                                               @RequestBody Cuenta cuenta) {

        try {
            this.servicioCliente.reemplazarCuentaCliente(documento, cuenta);
        } catch(Exception x) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }


    @PatchMapping
    public ResponseEntity emparcharCuentaCliente(@PathVariable String documento,
                                                 @RequestBody Cuenta cuenta){
        try {
            this.servicioCliente.emparcharCuentaCliente(documento, cuenta);
        } catch(Exception x) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();

    }

    // DELETE http://localhost:9000/api/v1/clientes/12345678/cuentas/123456***
    @DeleteMapping("/{cuenta}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity borrarCuentasCliente(@PathVariable String documento,
                                               @PathVariable String cuenta){

        try {
            this.servicioCliente.borrarCuentasCliente(documento, cuenta);
        } catch(Exception x) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();


    }


}
